using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using System.IO;
using SimpleFileBrowser;

using Firebase;
using Firebase.Extensions;
using Firebase.Storage;

public class UploadFile : MonoBehaviour
{
	FirebaseStorage storage;
	StorageReference storageReference;
	void Start()
	{

		FileBrowser.SetFilters(true, new FileBrowser.Filter("Images", ".jpg", ".png"), new FileBrowser.Filter("Text Files", ".txt", ".pdf"));
		FileBrowser.SetExcludedExtensions(".lnk", ".tmp", ".zip", ".rar", ".exe");

		//FileBrowser.SetDefaultFilter("");
		//FileBrowser.AddQuickLink("Users", "D:/UnityProjects/AssetsForCloud/Assets/Cube.prefab", null);
		
		storage = FirebaseStorage.DefaultInstance;
		storageReference = storage.GetReferenceFromUrl("gs://roofstack-24061.appspot.com/");




	}
	public void OnButtonClick()
    {
		StartCoroutine(ShowLoadDialogCoroutine());
	}
	IEnumerator ShowLoadDialogCoroutine()
	{
		
		yield return FileBrowser.WaitForLoadDialog(FileBrowser.PickMode.FilesAndFolders, true, null, null, "Load Files and Folders", "Load");

		
		Debug.Log(FileBrowser.Success);

		if (FileBrowser.Success)
		{
			
			for (int i = 0; i < FileBrowser.Result.Length; i++)
				Debug.Log(FileBrowser.Result[i]);

			
			byte[] bytes = FileBrowserHelpers.ReadBytesFromFile(FileBrowser.Result[0]);

			string fileName = FileBrowserHelpers.GetFilename(FileBrowser.Result[0]);
			
			StorageReference uploadRef = storageReference.Child("uploads/" + fileName);
			Debug.Log("File upload started to " + uploadRef);


			uploadRef.PutBytesAsync(bytes).ContinueWithOnMainThread((task) => {
				if (task.IsFaulted || task.IsCanceled)
				{
					Debug.Log(task.Exception.ToString());
				}
				else
				{
					Debug.Log("File Uploaded Succesfully");
				}
			});
		
		}
	}
}
