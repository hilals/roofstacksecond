# README #

Read Me

This project has been built for RoofStack, as an AR project with Android Platform.

This project has been divided into two Unity projects, one for uploading to the cloud(AssetForCloud) and one for downloading from the cloud(URPAR Game).

As for the cloud system, I’ve used Firebase, both projects contain firebase sdk and google-services.json.

FOR THE PROJECT NAMED “RoofStackSecond”

The main script it uses is, UploadFile under the Scripts folder.

Under the Editor folder there is a CreateAssetBundles folder to convert assets for platform specific asset bundles.

If you press play and then UPLOAD you can browse on the computer and click the files you’d like to upload to the server.
